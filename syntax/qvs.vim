" Vim syntax file
" Language:    Qlikview
" Maintainer:  Christian Stickel Nogueira

if version < 600
    syntax clear
elseif exists("b:current_syntax")
    finish
endif

syn case ignore

syn match qvsStatement 'mapping\s\+load'
syn match qvsStatement 'noconcatenate\s\+load'
syn match qvsStatement 'left\s\+join\s\+load'
syn match qvsStatement 'inner\s\+join'
syn match qvsStatement 'drop\s\+field'
syn match qvsStatement 'drop\s\+fields'
syn match qvsStatement 'group\s\+by'
syn match qvsStatement 'load'

syn keyword qvsStatement crosstable concatenate 
syn keyword qvsStatement FROM FOR IF END ELSE STORE DROP NEXT SUB FIRST TO TABLE THEN INLINE RESIDENT WHERE INTO EXECUTE DISTINCT
syn keyword qvsKeyItems SET LET EXIT


syn keyword qvsFunction sum min max only mode firstsortedvalue hash256
syn keyword qvsFunction MinString MaxString FirstValue LastValue concat
syn keyword qvsFunction count NumericCount TextCount NullCount MissingCount
syn keyword qvsFunction fractile kurtosis correl avg stdev skew median sterr steyx linest_m linest_b linest_r2 linest_sem linest_seb linest_sey linest_df linest_f linest_ssreg linest_ssresid
syn keyword qvsFunction irr xirr npv xnpv
syn keyword qvsFunction chi2test_p chi2test_df chi2test_chi2 TTest_t TTest_df TTest_sig TTest_dif TTest_sterr TTest_conf TTest_lower TTest_upper TTestw_t TTestw_df TTestw_sig TTestw_dif
syn keyword qvsFunction TTestw_sterr TTestw_conf TTestw_lower TTestw_upper TTest1_t TTest1_df TTest1_sig TTest1_dif TTest1_sterr TTest1_conf TTest1_lower TTest1_upper TTest1w_t TTest1w_df
syn keyword qvsFunction TTest1w_sig TTest1w_dif TTest1w_sterr TTest1w_conf TTest1w_lower TTest1w_upper ZTest_z ZTest_sig ZTest_dif ZTest_sterr ZTest_conf ZTestw_z ZTestw_sig ZTestw_dif
syn keyword qvsFunction ZTestw_sterr ZTestw_conf

syn keyword qvsFunction div mod fmod ceil floor frac round fabs numsum numcount numavg nummin nummax fact combin permut even odd sign bitcount
syn keyword qvsFunction rangesum rangeavg rangecount rangemin rangemax rangestdev rangeskew rangekurtosis rangefractile rangenumericcount rangetextcount rangenullcount rangemissingcount
syn keyword qvsFunction rangeminstring rangeminstring rangemode rangeonly rangecorrel rangeirr rangenpv rangexirr rangexnpv
syn keyword qvsFunction exp log log10 sqrt sqr pow
syn keyword qvsFunction cos acos sin asin tan atan atan2 cosh sinh tanh
syn keyword qvsFunction chidist chiinv normdist norminv tdist tinv fdist finv
syn keyword qvsFunction fv nper pmt pv rate BlackAndSchole
syn keyword qvsFunction e pi rand true false and
syn keyword qvsFunction RecNo RowNo IterNo autonumber autonumberhash128 autonumberhash256 fieldvaluecount
syn keyword qvsFunction ord chr len left right mid index upper lower repeat ltrim rtrim subfield KeepChar PurgeChar capitalize evaluate TextBetween Replace FindOneOf hash128 hash160 hash256 trim
syn keyword qvsFunction substringcount applycodepage
syn keyword qvsFunction applymap mapsubstring
syn keyword qvsFunction exists previous peek FieldValue FieldIndex lookup
syn keyword qvsFunction alt pick match mixmatch wildmatch class
syn keyword qvsFunction IsNum IsText IsPartialReload Null IsNull
syn keyword qvsFunction ClientPlatform OSuser QVuser ComputerName ReloadTime GetActiveSheetID GetCurrentField GetFieldSelections GetSelectedCount GetPossibleCount GetExcludedCount
syn keyword qvsFunction GetAlternativeCount GetNotSelectedCount GetRegistryString qlikviewversion MsgBox Input DocumentName DocumentPath DocumentTitle GetObjectField GetExtendedProperty
syn keyword qvsFunction Attribute ConnectString filebasename filedir fileextension filename filepath filesize filetime GetFolderPath QvdCreateTime QvdNoOfRecords QvdNoOfFields QvdFieldName
syn keyword qvsFunction QvdTableName
syn keyword qvsFunction FieldName FieldNumber NoOfFields NoOfRows NoOfTables TableName TableNumber
syn keyword qvsFunction TableNumber ReportName ReportID ReportNumber NoOfReports

syn keyword qvsFunction second minute hour day week month year weekyear weekday now today LocalTime MakeDate MakeWeekDate MakeTime AddMonths YearToDate TimeZone GMT UTC DaylightSaving
syn keyword qvsFunction SetDateYear SetDateYearMonth InYear InYearToDate InQuarter InQuarterToDate InMonth InMonthToDate InMonths InMonthsToDate InWeek InWeekToDate InLunarWeek
syn keyword qvsFunction InLunarWeekToDate InDay InDayToTime YearStart YearName QuarterStart QuarterEnd QuarterName MonthStart MonthEnd MonthName MonthsStart MonthsEnd MonthsName
syn keyword qvsFunction WeekStart WeekEnd WeekName LunarweekStart LunarweekEnd LunarWeekName DayStart DayEnd DayName age networkdays firstworkdate lastworkdate ConvertToLocalTime
syn keyword qvsFunction DayNumberOfYear DayNumberOfQuarter
syn keyword qvsFunction ThousandSep DecimalSep MoneyThousandSep MoneyDecimalSep MoneyFormat DirectMoneyDecimalSep DirectMoneyFormat TimeFormat DateFormat TimestampFormat MonthNames
syn keyword qvsFunction LongMonthNames DayNames LongDayNames
syn keyword qvsFunction Num Money Date Time Dual Interval timestamp

syn match qvsFunction "\<\k\+\ze("

" Strings and characters
syn region qvsString            start=+'+    end=+'+
syn region qvsVariable          start=+$(+ end=+)+

" Numbers
syn match qvsNumber             '[-+]\d\+\.\d*'

" Comments
syn region qvsComment           start=/\/\// end=/$/
syn region qvsMultiComment      start="/\*" end="\*/" 

hi def link qvsComment          Comment
hi def link qvsMultiComment     Comment
hi def link qvsNumber           Number
hi def link qvsString           String
hi def link qvsKeyItems         Keyword
hi def link qvsStatement        Statement
hi def link qvsMathConstants    Statement
hi def link qvsSystemVariables  Statement
hi def link qvsVariable         Include
hi def link qvsFunction        Function

let b:current_syntax = "qvs"
